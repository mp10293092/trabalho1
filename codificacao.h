#include <stdio.h>
#include <stdint.h>

#include "wave_reader.h"

enum IdentificadorCodificador {
    /* Observação há estados intermediários, aplicar | para combinar codificadores */
    CODIFICADOR_NENHUM = 0x0,
    CODIFICADOR_DIFERENCA = 0x1,
    CODIFICADOR_CARREIRA = 0x2,
    CODIFICADOR_HUFFMAN = 0x4
};

typedef struct ST_COMPRESSION_HEADER {
    enum IdentificadorCodificador id;
    wav_hdr wav_header;
} cmp_hdr;

typedef struct ST_HUFF_PAIR {
    char symbol;
    uint16_t frequency;
} huff_pair;

typedef struct ST_HUFF_NODE {
    char *bit_string;
    huff_pair *pair;
    struct ST_HUFF_NODE *left;
    struct ST_HUFF_NODE *right;
} huff_node;

typedef struct ST_HUFF_CODE {
    char symbol;
    char *bit_string;
} huff_code;

int read_header(FILE *in, cmp_hdr *header);

int write_header(FILE *out, cmp_hdr *header);

int difference(FILE *in, FILE *out);

int difference_decompress(FILE *in, FILE *out);

int improved_difference(FILE *in, FILE *out, wav_hdr *header);

int improved_difference_decompress(FILE *in, FILE *out, wav_hdr *header);

int run_length(FILE *in, FILE *out);

int run_length_decompress(FILE *in, FILE *out);

int huff_pair_compare_symbol(const void *pa, const void *pb);

int huff_pair_compare_frequency(const void *pa, const void *pb);

int huffman(FILE *in, FILE *out);

int huffman_decompress(FILE *in, FILE *out);
