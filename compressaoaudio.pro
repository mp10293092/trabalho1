TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

QMAKE_CC = gcc
QMAKE_CFLAGS += -ansi
QMAKE_LINK = gcc
LIBS += -lm

SOURCES += argumentos.c \
    codificacao.c \
    wave_reader.c

HEADERS += \
    argumentos.h \
    codificacao.h \
    wave_reader.h

DISTFILES += \
    estruturaCabeçalho \
    Makefile

configCompressor {
    SOURCES += compressor.c
    TARGET = compressor
}

configDecompressor {
    SOURCES += decompressor.c
    TARGET = decompressor
}
