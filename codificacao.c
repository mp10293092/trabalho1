#include "codificacao.h"
#include <stdio.h>
#include <stdint.h>
#include <limits.h>
#include <assert.h>
#include <string.h>
#include <math.h>

char char_getBit(char symbol, int bit) {
    return (symbol >> bit) & 0x01;
}

char strbyte2bit(char *byte) {
    int i=0;
    char value=0;
    for(i=0; i<8; i++) {
        int b = byte[i] - 48;
        value += b*pow(2, (8-i-1));
    }
    return value;
}

char* str2bit(char *str) {
    int len = strlen(str);

    /* Fix odd number */
    int rest = len%8;
    if(rest != 0) {
        int div = len/8;
        int newLen = (div+1)*8;
        str = realloc(str, sizeof(char)*newLen);

        int i=0;
        for(i=len; i<newLen; i++)
            str[i] = '0';
    }

    /* Convert str bits to char bits */
    char *bitstring = malloc(len/8);
    int i=0;
    for(i=0; i<len; i+=8) {
        char *c = &(str[i]);
        char value = strbyte2bit(c);
        bitstring[i/8] = value;
    }
    return bitstring;
}

int read_header(FILE *in, cmp_hdr *header) {
    fread(header, sizeof(*header), 1, in);
    return 1;
}

int write_header(FILE *out, cmp_hdr *header) {
    fwrite(header, sizeof(*header), 1, out);
    return 1;
}

int difference(FILE *in, FILE *out) {
    char last, current;
    char diff;
    size_t ret;

    ret = fread(&current, sizeof(current), 1, in);
    assert(ret == 1);
    fwrite(&current, sizeof(current), 1, out);
    last = current;
    while(fread(&current, sizeof(current), 1, in) == 1) {
        diff = current - last;
        fwrite(&diff, sizeof(diff), 1, out);
        last = current;
    }

    return 1;
}

char *difference_buffer_samples(FILE *in, size_t *pbuffer_size) {
    size_t samples = 0, buffer_size = 10;
    char *buffer = (char *) malloc(buffer_size * sizeof(buffer[0]));

    while(fread(&buffer[samples], sizeof(buffer[0]), 1, in) == 1) {
        samples++;
        if(samples == buffer_size) {
            buffer_size *= 2;
            buffer = (char *) realloc(buffer, buffer_size * sizeof(buffer[0]));
        }
    }

    *pbuffer_size = samples;

    return buffer;
}

char *difference_separate_channels(char *pbuffer, size_t pbuffer_size, size_t bitsPerSample) {
    size_t i, channel_buffer_size = pbuffer_size/2;

    if(bitsPerSample == 16) {
        int16_t *buffer = (int16_t *)pbuffer;
        int16_t *channel_buffer = (int16_t *) malloc(channel_buffer_size * sizeof(channel_buffer[0]));

        for(i = 1; i < channel_buffer_size; i += 2) {
            channel_buffer[i/2] = buffer[i];
            buffer[i/2] = buffer[i-1];
        }

        return (char *)channel_buffer;
    } else {
        char *channel_buffer = (char *) malloc(channel_buffer_size * sizeof(channel_buffer[0]));

        for(i = 1; i < pbuffer_size; i += 2) {
            channel_buffer[i/2] = pbuffer[i];
            pbuffer[i/2] = pbuffer[i-1];
        }

        return channel_buffer;
    }

    return NULL;
}

char *difference_join_channels(char *channel1, char *channel2, size_t channel_size, size_t bitsPerSample) {
    size_t i;

    if(bitsPerSample == 16) {
        int16_t *pchannel1 = (int16_t *)channel1;
        int16_t *pchannel2 = (int16_t *)channel2;
        int16_t *joined = (int16_t *)malloc(2 * channel_size * sizeof(pchannel1[0]));

        for(i = 0; i < channel_size; ++i) {
            joined[2*i] = pchannel1[i];
            joined[2*i+1] = pchannel2[i];
        }

        return (char *)joined;
    } else {
        char *joined = (char *) malloc(2 * channel_size * sizeof(channel1[0]));

        for(i = 0; i < channel_size; ++i) {
            joined[2*i] = channel1[i];
            joined[2*i+1] = channel2[i];
        }

        return joined;
    }

    return NULL;
}

size_t difference_bits_to_byte(char *bits, size_t total_bits) {
    size_t i, j;

    /* Organizar em bytes */
    char byte;
    size_t remaining_bits = total_bits % 8;
    size_t total_bytes = total_bits / 8;
    for(i = 0; i < total_bytes; ++i) {
        byte = 0;
        for(j = 0; j < 8; ++j)
            byte |= (bits[8*i+j] << j);
        bits[i] = byte;
    }
    if(remaining_bits > 0) {
        byte = 0;
        for(j = 0; j < remaining_bits; ++j)
            byte |= (bits[8*total_bytes+j] << j);
        bits[total_bytes] = byte;
        ++total_bytes;
    }

    return total_bytes;
}

char *difference_encode_channel16(int16_t *buffer, size_t buffer_size, size_t *ptotal_bytes) {
    size_t total_bits, total_bytes, diff_size = buffer_size - 1;
    size_t i, minIdx, maxIdx;
    uint16_t amplitude, *relative_diff;
    int16_t *diff = (int16_t *) malloc(diff_size * sizeof(diff[0]));
    int16_t minDiff;
    char *bits;

    /* Determinar diferenças e maior diferença */
    minIdx = 0;
    maxIdx = 0;
    for(i = 0; i < diff_size; ++i) {
        diff[i] = buffer[i+1] - buffer[i];
        if(diff[i] < diff[minIdx])
            minIdx = i;
        if(diff[i] > diff[maxIdx])
            maxIdx = i;
    }

    /* Determinar tamanho em bits da maior diferença */
    unsigned char bit_per_symbol;
    amplitude = (int32_t)diff[maxIdx] - (int32_t)diff[minIdx];
    for(bit_per_symbol = 0; amplitude > 0; ++bit_per_symbol)
        amplitude >>= 1;

    /* Determinar total de bits e alocar espaço */
    total_bits = bit_per_symbol * diff_size;
    bits = (char *) malloc(sizeof(buffer[0]) + sizeof(diff[minIdx]) + sizeof(bit_per_symbol) + total_bits * sizeof(bits[0]));

    /* Inserir valor inicial */
    ((int16_t *)bits)[0] = buffer[0];

    /* Inserir valor de deslocamento, facilita armazenar as diferenças negativas */
    ((int16_t *)bits)[1] = diff[minIdx];

    /* Inserir número de bits por símbolo */
    ((unsigned char *)bits)[4] = bit_per_symbol;

    /* Calcular diferenças relativas */
    minDiff = diff[minIdx];
    relative_diff = (uint16_t *)diff;
    for(i = 0; i < diff_size; ++i)
        relative_diff[i] = (int32_t)diff[i] - (int32_t)minDiff;

    /* Extrair bits */
    for(i = 0; i < total_bits; ++i)
        bits[i+5] =  (relative_diff[i / 16] >> (i % 16)) & 1;

    total_bytes = difference_bits_to_byte(&bits[5], total_bits) + sizeof(buffer[0]) + sizeof(minDiff) + sizeof(bit_per_symbol);

    bits = (char *)realloc(bits, total_bytes);

    if(ptotal_bytes != NULL)
        *ptotal_bytes = total_bytes;

    free(diff);

    return bits;
}

int16_t *difference_decode_channel16(char *bytes, size_t total_bytes, size_t *channel_size) {
    unsigned char bit_per_symbol = ((unsigned char *)bytes)[4];
    uint16_t buffer;
    int16_t min_diff = ((int16_t *)bytes)[1];
    int16_t* diff;
    size_t i, j;
    size_t total_bits = 8 * (total_bytes - sizeof(diff[0]) - sizeof(min_diff) - sizeof(bit_per_symbol));
    size_t symbol_num = total_bits / bit_per_symbol;
    int16_t* channel = (int16_t *) malloc((symbol_num + 1) * sizeof(channel[0]));
    char *decompressed_bits = (char *) malloc(total_bits * sizeof(decompressed_bits[0]));

    channel[0] = ((int16_t *)bytes)[0]; /* Restaurar primeiro valor */

    /* Separar bits */
    bytes = &bytes[5];
    for(i = 0; i < total_bits; ++i)
        decompressed_bits[i] = (bytes[i/8] >> (i % 8)) & 1;

    /* Recompor bits em inteiros, compondo as diferenças */
    diff = &channel[1];
    for(i = 0; i < symbol_num; ++i) {
        buffer = 0;
        for(j = 0; j < bit_per_symbol; ++j)
            buffer |= ((uint16_t)decompressed_bits[i*bit_per_symbol + j]) << j;
        diff[i] = (int32_t)buffer + (int32_t)min_diff;
    }

    /* Recompor canal */
    for(i = 1; i < symbol_num + 1; ++i)
        channel[i] += channel[i-1]; /* Somar valor anterior com a diferença, para obter o atual */

    if(channel_size != NULL)
        *channel_size = symbol_num + 1;

    free(decompressed_bits);

    return channel;
}

char *difference_encode_channel8(char *buffer, size_t buffer_size, size_t *ptotal_bytes) {
    size_t total_bits, total_bytes, diff_size = buffer_size - 1;
    size_t i, minIdx, maxIdx;
    unsigned char amplitude, *relative_diff;
    char *diff = (char *) malloc(diff_size * sizeof(diff[0]));
    char minDiff;
    char *bits;

    /* Determinar diferenças e maior diferença */
    minIdx = 0;
    maxIdx = 0;
    for(i = 0; i < diff_size; ++i) {
        diff[i] = buffer[i+1] - buffer[i];
        if(diff[i] < diff[minIdx])
            minIdx = i;
        if(diff[i] > diff[maxIdx])
            maxIdx = i;
    }

    /* Determinar tamanho em bits da maior diferença */
    unsigned char bit_per_symbol;
    amplitude = (int16_t)diff[maxIdx] - (int16_t)diff[minIdx];
    for(bit_per_symbol = 0; amplitude > 0; ++bit_per_symbol)
        amplitude >>= 1;

    /* Determinar total de bits e alocar espaço */
    total_bits = bit_per_symbol * diff_size;
    bits = (char *) malloc(sizeof(buffer[0]) + sizeof(diff[minIdx]) + sizeof(bit_per_symbol) + total_bits * sizeof(bits[0]));

    /* Inserir valor inicial */
    bits[0] = buffer[0];

    /* Inserir valor de deslocamento, facilita armazenar as diferenças negativas */
    bits[1] = diff[minIdx];

    /* Inserir número de bits por símbolo */
    ((unsigned char *)bits)[2] = bit_per_symbol;

    /* Calcular diferenças relativas */
    minDiff = diff[minIdx];
    relative_diff = (unsigned char *)diff;
    for(i = 0; i < buffer_size; ++i)
        relative_diff[i] = (int16_t)diff[i] - (int16_t)minDiff;

    /* Extrair bits */
    for(i = 0; i < total_bits; ++i)
        bits[i+3] =  (relative_diff[i / 16] >> (i % 16)) & 1;

    total_bytes = difference_bits_to_byte(&bits[3], total_bits) + sizeof(buffer[0]) + sizeof(minDiff) + sizeof(bit_per_symbol);

    bits = (char *)realloc(bits, total_bytes);

    if(ptotal_bytes != NULL)
        *ptotal_bytes = total_bytes;

    free(diff);

    return bits;
}

char *difference_decode_channel8(char *bytes, size_t total_bytes, size_t *channel_size) {
    unsigned char bit_per_symbol = ((unsigned char *)bytes)[4];
    unsigned char buffer;
    char min_diff = ((char *)bytes)[1];
    char* diff;
    size_t i, j;
    size_t total_bits = 8 * (total_bytes - sizeof(diff[0]) - sizeof(min_diff) - sizeof(bit_per_symbol));
    size_t symbol_num = total_bits / bit_per_symbol;
    char* channel = (char *) malloc((symbol_num + 1) * sizeof(channel[0]));
    char *decompressed_bits = (char *) malloc(total_bits * sizeof(decompressed_bits[0]));

    channel[0] = bytes[0]; /* Restaurar primeiro valor */

    /* Separar bits */
    bytes = &bytes[3];
    for(i = 0; i < total_bits; ++i)
        decompressed_bits[i] = (bytes[i/8] >> (i % 8)) & 1;

    /* Recompor bits em inteiros, compondo as diferenças */
    diff = &channel[1];
    for(i = 0; i < symbol_num; ++i) {
        buffer = 0;
        for(j = 0; j < bit_per_symbol; ++j)
            buffer |= ((unsigned char)decompressed_bits[i*bit_per_symbol + j]) << j;
        diff[i] = (int16_t)buffer + (int16_t)min_diff;
    }

    /* Recompor canal */
    for(i = 1; i < symbol_num + 1; ++i)
        channel[i] += channel[i-1]; /* Somar valor anterior com a diferença, para obter o atual */

    if(channel_size != NULL)
        *channel_size = symbol_num + 1;

    free(decompressed_bits);

    return channel;
}

int improved_difference(FILE *in, FILE *out, wav_hdr *header) {
    char *buffer, *separate_channel, *bytes, *bytes2;
    size_t buffer_size, total_bytes;

    buffer = difference_buffer_samples(in, &buffer_size);

    if(header->NumOfChan == 2) {
        separate_channel = (char *)difference_separate_channels(buffer, buffer_size, header->bitsPerSample);
        if(header->bitsPerSample == 16) {
            bytes = difference_encode_channel16((int16_t *)buffer, buffer_size/4, &total_bytes);
            bytes2 = difference_encode_channel16((int16_t *)separate_channel, buffer_size/4, NULL);
        } else {
            bytes = difference_encode_channel8(buffer, buffer_size/2, &total_bytes);
            bytes2 = difference_encode_channel8(separate_channel, buffer_size/2, NULL);
        }
        free(separate_channel);
    } else {
        if(header->bitsPerSample == 16)
            bytes = difference_encode_channel16((int16_t *)buffer, buffer_size/2, &total_bytes);
        else
            bytes = difference_encode_channel8(buffer, buffer_size, &total_bytes);
    }

    fwrite(&total_bytes, sizeof(total_bytes), 1, out);
    fwrite(bytes, sizeof(bytes[0]), total_bytes, out);
    if(header->NumOfChan == 2) {
        fwrite(bytes2, sizeof(bytes2[0]), total_bytes, out);
        free(bytes2);
    }

    free(bytes);

    return 1;
}

int improved_difference_decompress(FILE *in, FILE *out, wav_hdr *header) {
    char *bytes, *bytes2;
    size_t total_bytes, channel_size;

    fread(&total_bytes, sizeof(total_bytes), 1, in);
    bytes = (char *) malloc(total_bytes * sizeof(bytes[0]));
    fread(bytes, sizeof(bytes[0]), total_bytes, in);
    if(header->NumOfChan == 2) {
        bytes2 = (char *) malloc(total_bytes * sizeof(bytes2[0]));
        fread(bytes2, sizeof(bytes2[0]), total_bytes, in);
    }

    if(header->bitsPerSample == 16) {
        int16_t *channel1 = difference_decode_channel16(bytes, total_bytes, &channel_size);
        if(header->NumOfChan == 2) {
            int16_t *channel2 = difference_decode_channel16(bytes2, total_bytes, NULL);
            free(bytes2);

            int16_t *joined = (int16_t *)difference_join_channels((char *)channel1, (char *)channel2, channel_size, 16);
            free(channel2);

            fwrite(joined, sizeof(joined[0]), 2 * channel_size, out);
            free(joined);
        }
        else
            fwrite(channel1, sizeof(channel1[0]), channel_size, out);

        free(channel1);
    } else {
        char *channel1 = difference_decode_channel8(bytes, total_bytes, &channel_size);
        if(header->NumOfChan == 2) {
            char *channel2 = difference_decode_channel8(bytes2, total_bytes, NULL);
            free(bytes2);

            char *joined = difference_join_channels(channel1, channel2, channel_size, 8);
            free(channel2);

            fwrite(joined, sizeof(joined[0]), 2 * channel_size, out);
            free(joined);
        }
        else
            fwrite(channel1, sizeof(channel1[0]), channel_size, out);

        free(channel1);
    }

    free(bytes);

    return 1;
}

int difference_decompress(FILE *in, FILE *out) {
    char last;
    char diff;
    size_t ret;

    ret = fread(&last, sizeof(last), 1, in);
    assert(ret == 1);
    fwrite(&last, sizeof(last), 1, out);
    while(fread(&diff, sizeof(diff), 1, in) == 1) {
        last += diff;
        fwrite(&last, sizeof(last), 1, out);
    }

    return 1;
}

void char2bitstring(char c, char *dest) {
    int i=0;
    for(i=7; i>=0; i--) {
        int bit = char_getBit(c, i);
        dest[7-i] = bit + 48;
    }
}

int run_length(FILE *in, FILE *out) {
    char current = -1, next;
    char occurences = 0;

    char symbol;
    while(fread(&symbol, sizeof(symbol), 1, in) == 1) {

        /* Process bit per bit */
        int i=0;
        for(i=7; i>=0; i--) {
            next = char_getBit(symbol, i);

            /* First bit */
            if(current==-1) {
                current = next;
                occurences = 1;

                /* First bit not zero */
                if(current!=0) {
                    char zero = 0;
                    fwrite(&zero, sizeof(zero), 1, out);
                }

                continue;

            } else {
                if(current==next) {
                    occurences++;
                    continue;
                } else {
                    fwrite(&occurences, sizeof(occurences), 1, out);

                    current = next;
                    occurences = 1;
                }

            }

        }

    }

    /* Last bit */
    fwrite(&occurences, sizeof(occurences), 1, out);

    return 1;
}

int run_length_decompress(FILE *in, FILE *out) {
    char *bitstring = malloc(sizeof(char));
    int len = 0;
    bitstring[0] = '\0';

    char occurences, current = 0;
    while(fread(&occurences, sizeof(occurences), 1, in) == 1) {

        /* Realloc bitstring*/
        int newLen = len + occurences;
        bitstring = (char *) realloc(bitstring, (newLen+1)*sizeof(char));
        bitstring[newLen] = '\0';
        len = newLen;

        /* Put values */
        memset(&(bitstring[newLen-occurences]), current+48, occurences);

        current = (current==0? 1 : 0);
    }

    /* Write to output*/
    char *decoded = str2bit(bitstring);
    fwrite(decoded, sizeof(char), strlen(bitstring)/8, out);

    return 1;
}

int huff_pair_compare_symbol(const void *pa, const void *pb) {
    huff_pair *a = (huff_pair *)pa;
    huff_pair *b = (huff_pair *)pb;

    if (a->symbol <  b->symbol)
        return -1;
    else if (a->symbol == b->symbol)
        return 0;
    else
        return 1;
}

huff_pair *huffman_generate_frequency(FILE *in, size_t *pSymbolNum) {
    size_t symbolNum = 0, symbolArraySize = 8;
    huff_pair *symbols = (huff_pair *) calloc(symbolArraySize, sizeof(huff_pair));
    huff_pair current, *search;

    while(fread(&(current.symbol), sizeof(current.symbol), 1, in) == 1) {
        current.frequency = 1;
        search = (huff_pair *)bsearch(
            (void *)&current, (void *)&symbols[0],
            symbolNum, sizeof(current),
            huff_pair_compare_symbol
        );

        /* Se o símbolo ainda não existe, insere no array */
        if(search == NULL) {
            /* Se não há espaço no array, aloca. */
            if(symbolNum >= symbolArraySize) {
                symbolArraySize *= 2;
                symbols = (huff_pair *) realloc(symbols, sizeof(symbols[0]) * symbolArraySize);
            }

            symbols[symbolNum] = current;
            ++symbolNum;

            /* Ordena após inserção */
            qsort(
                (void *)&symbols[0], symbolNum,
                sizeof(symbols[0]), huff_pair_compare_symbol
            );
        } else {
            search->frequency += 1;
        }
    }

    *pSymbolNum = symbolNum;

    return realloc(symbols, sizeof(symbols[0]) * symbolNum);
}

int huffman_node_compare_frequency(const void *pa, const void *pb) {
    huff_node *a = *(huff_node **)pa;
    huff_node *b = *(huff_node **)pb;

    if (a->pair->frequency < b->pair->frequency)
        return 1;
    if (a->pair->frequency == b->pair->frequency)
        return 0;
    return -1;
}

huff_node *huffman_build_leaf(huff_pair *symbol) {
    huff_node *node = (huff_node *) malloc(sizeof(huff_node));

    node->left = node->right = NULL;
    node->pair = symbol;
    node->bit_string = NULL;

    return node;
}

huff_node *huffman_join_nodes(huff_node *left, huff_node *right) {
    huff_node *new_node = (huff_node *) malloc(sizeof(huff_node));

    /* New pair */
    new_node->pair = (huff_pair *) malloc(sizeof(huff_pair));
    new_node->pair->symbol = '*';
    new_node->pair->frequency = left->pair->frequency + right->pair->frequency;

    /* Children nodes */
    new_node->left = left;
    new_node->right = right;

    return new_node;
}

huff_node* huffman_generate_tree(huff_pair *symbols, size_t symbolNum) {
    huff_node **ordered_queue = (huff_node **)malloc(sizeof(huff_node *) * symbolNum);
    huff_node **leaves = (huff_node **)malloc(sizeof(huff_node *) * symbolNum);

    /* Generate leaves */
    size_t i, queue_size;
    for(i = 0; i < symbolNum; ++i)
        leaves[i] = ordered_queue[i] = huffman_build_leaf(&symbols[i]);

    /* Join nodes until reach the end */
    queue_size = symbolNum;
    while(queue_size > 1) {
        /* Ordena fila pela frequencia do nó */
        qsort(
            (void *)&ordered_queue[0], queue_size,
            sizeof(ordered_queue[0]), huffman_node_compare_frequency
        );

        /* Combina os 2 itens menos frequentes */
        --queue_size;
        ordered_queue[queue_size - 1] = huffman_join_nodes(ordered_queue[queue_size], ordered_queue[queue_size - 1]);
    }

    return ordered_queue[0];
}

void huffman_generate_code(huff_node *node, char *currBitstring, huff_code ***codes, int *numCodes) {
    /* Check leave node */
    if(node->right==NULL && node->left==NULL) {
        /* Save on tree */
        node->bit_string = (char *) malloc(sizeof(currBitstring)+1);
        strcpy(node->bit_string, currBitstring);

        /* Create huff_code */
        huff_code *code = malloc(sizeof(huff_code));
        code->symbol = node->pair->symbol;

        code->bit_string = (char *) malloc(sizeof(currBitstring)+1);
        strcpy(code->bit_string, currBitstring);

        /* Add to codes */
        (*numCodes)++;
        *codes = (huff_code**) realloc(*codes, (*numCodes)*sizeof(huff_code*));
        (*codes)[(*numCodes)-1] = code;

    } else {
        /* Bitstring left */
        char *left = malloc(sizeof(char)*(strlen(currBitstring)+1));
        strcpy(left, currBitstring);
        strcat(left, "0");

        /* Bitstring right */
        char *right = malloc(sizeof(char)*(strlen(currBitstring)+1));
        strcpy(right, currBitstring);
        strcat(right, "1");

        /* Run recursion */
        if(node->left!=NULL)
            huffman_generate_code(node->left, left, codes, numCodes);
        if(node->right!=NULL)
            huffman_generate_code(node->right, right, codes, numCodes);
    }
}

char* huffman_symbol2code(huff_code **codes, int numCodes, char symbol) {
    int i=0;
    for(i=0; i<numCodes; i++) {
        if(codes[i]->symbol==symbol)
            return codes[i]->bit_string;
    }
    return NULL;
}

int huffman(FILE *in, FILE *out) {
    unsigned i=0;

    /* Generate symbol frequency */
    int cursor = ftell(in);

    size_t symbolNum = 0;
    huff_pair *symbols = huffman_generate_frequency(in, &symbolNum);
    fseek(in, cursor, SEEK_SET);

    /** DEBUG
    printf("Symbols / frequencies:\n");
    for(i=0; i<symbolNum; i++)
        printf("%02X -> %d\n", symbols[i].symbol, symbols[i].frequency);
    printf("\n");
    */

    /* Generate huffman tree */
    huff_node *tree = huffman_generate_tree(symbols, symbolNum);

    /* Generate leave nodes codes */
    int numCodes = 0;
    huff_code **codes = NULL;
    huffman_generate_code(tree, "", &codes, &numCodes);

    /** DEBUG
    printf("Codes / symbols:\n");
    for(i=0; i<symbolNum; i++)
        printf("%s -> %02X\n", codes[i]->bit_string, codes[i]->symbol);
    printf("\n");
    */

    /* Codifies input */
    int encodedSize = 0;
    char *encoded = malloc(sizeof(char));
    encoded[0] = '\0';

    char symbol;
    do {
        /* Read symbol */
        if(fread(&symbol, sizeof(symbol), 1, in) == 1) {
            /* Find code */
            char *code = huffman_symbol2code(codes, numCodes, symbol);

            /* Concat */
            encodedSize += strlen(code);
            encoded = realloc(encoded, sizeof(char)*(encodedSize+1));
            strcat(encoded, code);
        }
    } while(!feof(in));

    /** DEBUG
    printf("encoded: ");
    for(i=0; i<strlen(encoded); i++) {
        if(i%8==0)
            printf(" ");

        printf("%c", encoded[i]);

    }
    printf("\n");
    */

    /* Convert encoded to bitstring */
    char *bitstring = str2bit(encoded);

    /** DEBUG
    printf("encoded to bitstring: ");
    for(i=0; i<strlen(encoded)/8; i++) {
        char symbol = bitstring[i];
        printf("%02X", symbol & 0xFF);

        printf("(");

        int j = 0;
        for(j=7; j>=0; j--) {
            char bit = huffman_getBit(symbol, j);
            printf("%d", bit);
        }

        printf(") ");
    }
    printf("\n");
    */

    /* Write symbols frequency */
    fwrite(&symbolNum, sizeof(symbolNum), 1, out);

    for(i=0; i<symbolNum; i++) {
        char symbol = symbols[i].symbol;
        uint16_t frequency = symbols[i].frequency;

        fwrite(&symbol, sizeof(symbol), 1, out);
        fwrite(&frequency, sizeof(frequency), 1, out);
    }

    /* Write bitstring */
    fwrite(bitstring, sizeof(char), strlen(encoded)/8, out);

    return 1;
}

int huffman_decompress(FILE *in, FILE *out) {
    unsigned i=0;


    size_t symbolNum = 0;

    /* Read symbols frequency */
    fread(&symbolNum, sizeof(symbolNum), 1, in);
    huff_pair *symbols = (huff_pair*) malloc(sizeof(huff_pair)*symbolNum);
    for(i=0; i<symbolNum; i++) {
        fread(&(symbols[i].symbol), sizeof(symbols[i].symbol), 1, in);
        fread(&(symbols[i].frequency), sizeof(symbols[i].frequency), 1, in);
    }

    /* Regenerate huffman tree */
    huff_node *tree = huffman_generate_tree(symbols, symbolNum);

    /* Read bitstring and decode based on huff tree */
    huff_node *currNode = tree;
    char symbol;
    while(fread(&symbol, sizeof(symbol), 1, in) == 1) {

        /* Process bit per bit */
        int i=0;
        for(i=7; i>=0; i--) {
            int bit = char_getBit(symbol, i);

            /* Run on nodes (right/left) */
            if(bit) {
                if(currNode->right!=NULL)
                    currNode = currNode->right;
            } else {
                if(currNode->left!=NULL)
                    currNode = currNode->left;
            }

            /* Check if leave (decoded!) */
            if(currNode->right==NULL && currNode->left==NULL) {
                fwrite(&(currNode->pair->symbol), sizeof(char), 1, out);
                currNode = tree;
            }

        }

    }

    return 1;
}
