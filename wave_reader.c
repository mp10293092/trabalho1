#include "wave_reader.h"

/* find the file size */
long int getFileSize(FILE *inFile){
    long int fileSize = 0;
    fseek(inFile, 0, SEEK_END);

    fileSize = ftell(inFile);

    fseek(inFile, 0, SEEK_SET);
    return fileSize;
}

int print_wav_data(char *filename) {
    FILE *f;
    int headerSize = sizeof(wav_hdr);
    long int fileLength;
    wav_hdr header;

    /*
        if, for some reason, you are having problems to read WAVE fields,
        check whether the sizes of the following types are in accordance with
        the WAVE header specification:

    printf("char size: %lu\n", sizeof(char));
    printf("unsigned long size: %lu\n", sizeof(unsigned long));
    printf("unsigned short: %lu\n", sizeof(unsigned short));
    printf("unsigned int: %lu\n", sizeof(unsigned int));

    */

    if(filename == NULL) {
        printf("Usage: ./wave_reader file.wav\n");
        return EXIT_FAILURE;
    }

    f = fopen(filename, "r");
    if(f == NULL) {
        printf("Could not open wave file %s\n", filename);
        return EXIT_FAILURE;
    }

    fread(&header, headerSize, 1, f);
    fileLength = getFileSize(f);
    fclose(f);

    printf("File size: %ld bytes\n", fileLength);
    printf("ChunkID: %4s\n", header.RIFF);
    printf("ChunkSize: %u\n", header.ChunkSize);
    printf("AudioFormat: %d\n", header.AudioFormat);
    printf("NumOfChan: %d\n", header.NumOfChan);
    printf("SamplesPerSec: %u\n", header.SamplesPerSec);
    printf("bytesPerSec: %u\n", header.bytesPerSec);
    printf("bitsPerSample: %hu\n\n", header.bitsPerSample);

    return EXIT_SUCCESS;
}
