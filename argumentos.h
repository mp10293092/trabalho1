#include <argp.h>
#include "codificacao.h"

/* Atributos para documentação */
const char *argp_program_version;
const char * argp_program_bug_address;

/* Função do parser */
extern error_t parse_arguments(int key, char *arg, struct argp_state *state);

/* Estrutura que compõe o parser de argumentos */
extern const struct argp arg_parser;

/* Comunicação com a main */
struct argumentos {
    enum IdentificadorCodificador codificador;
    char *nome_entrada;
    char *nome_saida;
};
