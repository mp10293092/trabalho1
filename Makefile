all: encode decode

encode:
	gcc -ansi codificacao.c argumentos.c compressor.c wave_reader.c -o encode -lm

decode:
	gcc -ansi codificacao.c argumentos.c decompressor.c wave_reader.c -o decode -lm

clean:
	rm -f *.o
	rm -f encode
	rm -f decode

