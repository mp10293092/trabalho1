#include "argumentos.h"

static void debug_parser(struct argumentos *args) {
    printf("Arquivo de entrada: %s\n", args->nome_entrada);
    printf("Arquivo de saída: %s\n\n", args->nome_saida);
}

void apply_process(char *outname, FILE **in, int (*process)(FILE *in, FILE *out), int is_not_last) {
    FILE *out = fopen(outname, "w");
    process(*in, out);
    fclose(out);
    fclose(*in);

    if(is_not_last)
        *in = fopen(outname, "r");
    else
        rename(outname, "decoded");
}

void apply_difference(char *outname, FILE **in, wav_hdr *header, int is_not_last) {
    FILE *out = fopen(outname, "w");
    improved_difference_decompress(*in, out, header);
    fclose(out);
    fclose(*in);

    if(is_not_last)
        *in = fopen(outname, "r");
    else
        rename(outname, "decoded");
}

int main(int argc, char *argv[]) {

    struct argumentos args;
    args.codificador = CODIFICADOR_NENHUM;
    args.nome_entrada = "";
    args.nome_saida = "";

    argp_parse(&arg_parser, argc, argv, 0, NULL, (void *)&args);
    debug_parser(&args);

    FILE *out, *in = fopen(args.nome_entrada, "r");

    cmp_hdr header;
    read_header(in, &header);

    /* Desfazer huffman */
    if(header.id & CODIFICADOR_HUFFMAN) {
        printf("Decoding Huffman...\n");
        apply_process("decodeHuffmanOut", &in, huffman_decompress, (header.id & ~CODIFICADOR_HUFFMAN));
    }

    /* Desfazer carreira */
    if(header.id & CODIFICADOR_CARREIRA) {
        printf("Decoding Carreira...\n");
        apply_process("decodeRunLengthOut", &in, run_length_decompress, (header.id & ~CODIFICADOR_CARREIRA));
    }

    /* Desfazer diferenças */
    if(header.id & CODIFICADOR_DIFERENCA) {
        printf("Decoding Diferença...\n");
        apply_difference("decodeDiffOut", &in, &(header.wav_header), 0);
    }

    in = fopen("decoded", "r");
    out = fopen(args.nome_saida, "w");
    fwrite(&(header.wav_header), sizeof(header.wav_header), 1, out);

    char next;
    while(fread(&next, sizeof(next), 1, in) == 1)
        fwrite(&next, sizeof(next), 1, out);

    fclose(in);
    fclose(out);

    return 0;
}

error_t parse_arguments( int key, char *arg, struct argp_state *state ) {
    struct argumentos* a = (struct argumentos *)state->input;
    switch ( key ) {
        case ARGP_KEY_ARG:
            switch(state->arg_num) {
                case 0:
                    a->nome_entrada = arg;
                    break;
                case 1:
                    a->nome_saida = arg;
                    break;
            }
            break;
        default:
            break;
    }
    return 0;
}

const struct argp arg_parser = {
    /* Estrutura para processar os argumentos */
    .options = (const struct argp_option[]){
        { /* Nome do arquivo de entrada */
            .name = 0,
            .key = ARGP_KEY_ARG,
            .arg = "ENTRADA",
            .flags = 0,
            .doc = 0,
            .group = 0
        },
        { /* Nome do arquivo de saída */
            .name = 0,
            .key = ARGP_KEY_ARG,
            .arg = "SAÍDA",
            .flags = 0,
            .doc = 0,
            .group = 0
        },
        { 0 }
    },

    /* Função executada pelo parser */
    .parser = parse_arguments,

    /* Nomes dos argumentos aceitos */
    .args_doc = "ENTRADA SAÍDA",

    .doc = 0, /* Documentação extra */
    .children = 0,
    .help_filter = 0,
    .argp_domain = 0
};
