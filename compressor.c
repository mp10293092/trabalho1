#include <stdio.h>
#include <string.h>

/* Processar argumentos */
#include "argumentos.h"

static void debug_parser(struct argumentos *args) {
    printf("Arquivo de entrada: %s\n", args->nome_entrada);
    printf("Arquivo de saída: %s\n\n", args->nome_saida);
}

void apply_process(char *outname, FILE **in, int (*process)(FILE *in, FILE *out), int is_not_last) {
    FILE *out = fopen(outname, "w");
    process(*in, out);
    fclose(out);
    fclose(*in);

    if(is_not_last)
        *in = fopen(outname, "r");
    else
        rename(outname, "encoded");
}

void apply_difference(char *outname, FILE **in, wav_hdr *header, int is_not_last) {
    FILE *out = fopen(outname, "w");
    improved_difference(*in, out, header);
    fclose(out);
    fclose(*in);

    if(is_not_last)
        *in = fopen(outname, "r");
    else
        rename(outname, "encoded");
}

int main(int argc, char *argv[]) {
    struct argumentos args;
    args.codificador = CODIFICADOR_NENHUM;
    args.nome_entrada = "";
    args.nome_saida = "";

    argp_parse(&arg_parser, argc, argv, 0, NULL, (void *)&args);
    debug_parser(&args);

    cmp_hdr header;
    header.id = args.codificador;
    FILE *out, *in = fopen(args.nome_entrada, "r");
    fread(&(header.wav_header), sizeof(header.wav_header), 1, in);

    /* Aplicar diferenças */
    if(args.codificador & CODIFICADOR_DIFERENCA) {
        printf("Encoding Diferença...\n");
        apply_difference("encodeDiffOut", &in, &(header.wav_header), args.codificador != CODIFICADOR_DIFERENCA);
    }

    /* Aplicar carreira */
    if(args.codificador & CODIFICADOR_CARREIRA) {
        printf("Encoding Carreira...\n");
        apply_process("encodeRunLengthOut", &in, run_length, args.codificador & CODIFICADOR_HUFFMAN);
    }

    /* Aplicar huffman */
    if(args.codificador & CODIFICADOR_HUFFMAN) {
        printf("Encoding Huffman...\n");
        apply_process("encodeHuffmanOut", &in, huffman, 0);
    }

    in = fopen("encoded", "r");
    out = fopen(args.nome_saida, "w");
    write_header(out, &header);

    char next;
    while(fread(&next, sizeof(next), 1, in) == 1)
        fwrite(&next, sizeof(next), 1, out);

    fclose(in);
    fclose(out);

    return 0;
}

/* Função do parser */
error_t parse_arguments( int key, char *arg, struct argp_state *state ) {
    struct argumentos* a = (struct argumentos *)state->input;
    switch ( key ) {
        case 'd':
            a->codificador |= CODIFICADOR_DIFERENCA;
            break;
        case 'c':
            a->codificador |= CODIFICADOR_CARREIRA;
            break;
        case 'h':
            a->codificador |= CODIFICADOR_HUFFMAN;
            break;
        case ARGP_KEY_ARG:
            switch(state->arg_num) {
                case 0:
                    a->nome_entrada = arg;
                    break;
                case 1:
                    a->nome_saida = arg;
                    break;
            }
            break;
        default:
            break;
    }
    return 0;
}

/* Estrutura que compõe o parser de argumentos */
const struct argp arg_parser = {
    /* Estrutura para processar os argumentos */
    .options = (const struct argp_option[]){
        { /* Codificação por diferenças */
            .name = 0,
            .key = 'd',
            .arg = 0,
            .flags = OPTION_ARG_OPTIONAL,
            .doc = "utilização da codificação por diferenças",
            .group = 0
        },
        { /* Codificação por carreira */
            .name = 0,
            .key = 'c',
            .arg = 0,
            .flags = OPTION_ARG_OPTIONAL,
            .doc = "utilização da codificação por carreira",
            .group = 0
        },
        { /* Codificação por Huffman */
            .name = 0,
            .key = 'h',
            .arg = 0,
            .flags = OPTION_ARG_OPTIONAL,
            .doc = "utilização da codificação por Huffman",
            .group = 0
        },
        { /* Nome do arquivo de entrada */
            .name = 0,
            .key = ARGP_KEY_ARG,
            .arg = "ENTRADA",
            .flags = 0,
            .doc = 0,
            .group = 0
        },
        { /* Nome do arquivo de saída */
            .name = 0,
            .key = ARGP_KEY_ARG,
            .arg = "SAÍDA",
            .flags = 0,
            .doc = 0,
            .group = 0
        },
        { 0 }
    },

    /* Função executada pelo parser */
    .parser = parse_arguments,

    /* Nomes dos argumentos aceitos */
    .args_doc = "ENTRADA SAÍDA",

    .doc = 0, /* Documentação extra */
    .children = 0,
    .help_filter = 0,
    .argp_domain = 0
};
